package Modelo;

public class CuentaBancaria {
    private int numeroCuenta;
    private Cliente cliente;
    private String fecha;
    private String nombreBanco;
    private float porcentajeRendimiento;
    private float saldo;
    
    public CuentaBancaria(){
        
    }
    
    public CuentaBancaria(int numeroCuenta, Cliente cliente, String fecha, String nombreBanco, float porcentajeRendimiento, float saldo){
        this.numeroCuenta = numeroCuenta;
        this.cliente = cliente;
        this.fecha = fecha;
        this.nombreBanco = nombreBanco;
        this.porcentajeRendimiento = porcentajeRendimiento;
        this.saldo = saldo;
    }
    
    public CuentaBancaria(CuentaBancaria cuenta){
        
    }
    
    public void setNumeroCuenta(int numero){
        this.numeroCuenta = numero;
    }
    
    public int getNumeroCuenta(){
        return numeroCuenta;
    }
    
    public void setCliente(Cliente cliente){
        this.cliente = cliente;
    }
    
    public Cliente getCliente(){
        return cliente;
    }
    
    public void setFecha(String fecha){
        this.fecha = fecha;
    }
    
    public String getFecha(){
        return fecha;
    }
    
    public void setNombreBanco(String nombreBanco){
        this.nombreBanco = nombreBanco;
    }
    
    public String getNombreBanco(){
        return nombreBanco;
    }
    
    public void setPorcentajeRendimiento(float porcentaje){
        this.porcentajeRendimiento = porcentaje;
    }
    
    public float getPorcentajeRendimiento(){
        return porcentajeRendimiento;
    }
    
    public void setSaldo(float saldo){
        this.saldo = saldo;
    }
    
    public float getSaldo(){
        return saldo;
    }
    
    public void depositar(float deposito){
        saldo += deposito;
    }
    
    public boolean retirar(float retiro){
        if(retiro <= this.saldo){
            saldo -= retiro;
            return true;
        }
        else{
            return false;
        }
    }
    
    public float calcularRendimiento(){
        float rendimiento;
        rendimiento = (this.getPorcentajeRendimiento()*this.getSaldo())/365;
        return rendimiento;
    }
}
