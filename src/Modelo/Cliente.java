package Modelo;

public class Cliente {
    private String nombreCliente;
    private String fechaNacimiento;
    private String domicilio;
    private String sexo;
    
    public Cliente(){
        
    }
    
    public Cliente(String nombreCliente, String fechaNacimiento, String domicilio, String sexo){
        this.nombreCliente = nombreCliente;
        this.fechaNacimiento = fechaNacimiento;
        this.domicilio = domicilio;
        this.sexo = sexo;
    }
    
    public Cliente(Cliente cliente){
        this.nombreCliente = cliente.nombreCliente;
        this.fechaNacimiento = cliente.fechaNacimiento;
        this.domicilio = cliente.domicilio;
        this.sexo = cliente.sexo;
    }
    
    public void setNombreCliente(String nombre){
        this.nombreCliente = nombre;
    }
    
    public String getNombreCliente(){
        return nombreCliente;
    }
    
    public void setFechaNacimiento(String fechaNacimiento){
        this.fechaNacimiento = fechaNacimiento;
    }
    
    public String getFechaNacimiento(){
        return fechaNacimiento;
    }
    
    public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }
    
    public String getDomicilio(){
        return domicilio;
    }
    
    public void setSexo(String sexo){
        this.sexo = sexo;
    }
    
    public String getSexo(){
        return sexo;
    }
}
