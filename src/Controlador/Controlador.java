package Controlador;
import Modelo.Cliente;
import Modelo.CuentaBancaria;
import Vista.dlgBanco;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;


public class Controlador implements ActionListener{
    

    private Cliente cliente;
    private CuentaBancaria cuenta;
    private dlgBanco vista;
    
    
    public Controlador(Cliente cliente, CuentaBancaria cuenta, dlgBanco vista){
        
        this.cliente = cliente;
        this.cuenta = cuenta;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnRetirar.addActionListener(this);
        vista.btnDepositar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.radioFemenino.addActionListener(this);
        vista.radioMasculino.addActionListener(this);
    }
    
    private void iniciarVista(){
       vista.setTitle("Banco");
       vista.setSize(520,530);
       vista.setLocationRelativeTo(null);
       vista.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
       vista.setVisible(true);
       vista.dispose();
       System.exit(0);
   }
    
    @Override
   public void actionPerformed(ActionEvent e) {
       
       if(e.getSource() == vista.btnCerrar){
           int result = JOptionPane.showConfirmDialog(vista, "¿Seguro que deseas salir?", "Advertencia", JOptionPane.YES_NO_OPTION);
           if (result == JOptionPane.YES_OPTION)
               System.exit(0);
           else if (result == JOptionPane.NO_OPTION)
               vista.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
       }
       
        if(e.getSource() == vista.btnCancelar){
           vista.txtCantidadMovimiento.setText("");
           vista.txtCantidadMovimiento.setEnabled(false);
           vista.txtDomicilio.setText("");
           vista.txtDomicilio.setEnabled(false);
           vista.txtFechaNacimiento.setText("");
           vista.txtFechaNacimiento.setEnabled(false);
           vista.txtNombreCliente.setText("");
           vista.txtNombreCliente.setEnabled(false);
           vista.txtNuevoSaldo.setText("");
           vista.txtNuevoSaldo.setEnabled(false);
           vista.txtNumeroCuenta.setText("");
           vista.txtNumeroCuenta.setEnabled(false);
           vista.txtPorcentajeRendimiento.setText("");
           vista.txtPorcentajeRendimiento.setEnabled(false);
           vista.txtSaldo.setText("");
           vista.txtSaldo.setEnabled(false);
           vista.txtNombreBanco.setText("");
           vista.txtNombreBanco.setEnabled(false);
           vista.txtFechaApertura.setText("");
           vista.txtFechaApertura.setEnabled(false);
           vista.btnSexos.clearSelection();
           vista.btnNuevo.setEnabled(true);
           vista.btnMostrar.setEnabled(false);
       }
       
       if(e.getSource() == vista.btnNuevo){
           vista.txtDomicilio.setEnabled(true);
           vista.txtFechaNacimiento.setEnabled(true);
           vista.txtNombreCliente.setEnabled(true);
           vista.txtNumeroCuenta.setEnabled(true);
           vista.txtPorcentajeRendimiento.setEnabled(true);
           vista.radioFemenino.setEnabled(true);
           vista.radioMasculino.setEnabled(true);
           vista.txtNombreBanco.setEnabled(true);
           vista.txtSaldo.setEnabled(true);
           vista.txtFechaApertura.setEnabled(true);
           if(cuenta.getSaldo()!=0){
               vista.btnMostrar.setEnabled(true);
           }
       }
       
       if(e.getSource() == vista.btnLimpiar){
           vista.txtCantidadMovimiento.setText("");
           vista.txtDomicilio.setText("");
           vista.txtFechaNacimiento.setText("");
           vista.txtNombreCliente.setText("");
           vista.txtNuevoSaldo.setText("");
           vista.txtNumeroCuenta.setText("");
           vista.txtPorcentajeRendimiento.setText("");
           vista.txtSaldo.setText("");
           vista.txtNombreBanco.setText("");
           vista.txtFechaApertura.setText("");
           vista.btnSexos.clearSelection();
       }
       
      
       if (e.getSource() == vista.radioMasculino) {
            cliente.setSexo(vista.radioMasculino.getText());
       } 
       else if (e.getSource() == vista.radioFemenino){
            cliente.setSexo(vista.radioFemenino.getText());
       }
       
        if (e.getSource() == vista.btnGuardar) {
            try {
                cuenta.setNumeroCuenta(Integer.parseInt(vista.txtNumeroCuenta.getText()));
                cuenta.setSaldo(Float.parseFloat(vista.txtSaldo.getText()));
                cuenta.setPorcentajeRendimiento(Float.parseFloat(vista.txtPorcentajeRendimiento.getText()));
            } catch (NumberFormatException exc) {
                JOptionPane.showMessageDialog(vista, "Error en la conversion de numero " + exc.getMessage());
            } finally {
                cliente.setDomicilio(vista.txtDomicilio.getText());
                cliente.setFechaNacimiento(vista.txtFechaNacimiento.getText());
                cliente.setNombreCliente(vista.txtNombreCliente.getText());
                cuenta.setNombreBanco(vista.txtNombreBanco.getText());
                cuenta.setCliente(cliente);
                cuenta.setFecha(vista.txtFechaApertura.getText());

                if (cliente.getDomicilio() != null && cliente.getFechaNacimiento() != null && cliente.getNombreCliente() != null && cliente.getSexo() != null && cuenta.getSaldo() != 0) {
                    vista.btnDepositar.setEnabled(true);
                    vista.btnRetirar.setEnabled(true);
                    vista.txtCantidadMovimiento.setEnabled(true);
                    vista.txtNuevoSaldo.setText(Float.toString(cuenta.getSaldo()));
                    vista.txtNuevoSaldo.setDisabledTextColor(Color.black);
                    JOptionPane.showMessageDialog(vista, "Cuenta creada correctamente ");
                    vista.btnNuevo.setEnabled(false);
                    vista.btnGuardar.setEnabled(false);
                }
            }
        }
       
        if (e.getSource() == vista.btnDepositar) {
            try {
                cuenta.depositar(Float.parseFloat(vista.txtCantidadMovimiento.getText()));
                vista.txtNuevoSaldo.setText(Float.toString(cuenta.getSaldo()));
            } catch (NumberFormatException exc) {
                JOptionPane.showMessageDialog(vista, "Error en la conversion de numero " + exc.getMessage());
            } finally {
                JOptionPane.showMessageDialog(vista, "Deposito Exitoso ");
            }
        }
       
       if(e.getSource() == vista.btnRetirar){
           if(!(cuenta.retirar(Float.parseFloat(vista.txtCantidadMovimiento.getText())))){
               JOptionPane.showMessageDialog(vista, "ERROR Saldo Insuficiente ");
           } else {
               vista.txtNuevoSaldo.setText(Float.toString(cuenta.getSaldo()));
               JOptionPane.showMessageDialog(vista, "Retiro completado ");
           }
       }
       
       if (e.getSource() == vista.btnMostrar){
           vista.txtDomicilio.setText(cliente.getDomicilio());
           vista.txtFechaNacimiento.setText(cliente.getFechaNacimiento());
           vista.txtFechaApertura.setText(cuenta.getFecha());
           vista.txtNombreBanco.setText(cuenta.getNombreBanco());
           vista.txtNombreCliente.setText(cliente.getNombreCliente());
           vista.txtNuevoSaldo.setText(Float.toString(cuenta.getSaldo()));
           vista.txtNumeroCuenta.setText(Integer.toString(cuenta.getNumeroCuenta()));
           vista.txtPorcentajeRendimiento.setText(Float.toString(cuenta.getPorcentajeRendimiento()));
           vista.txtSaldo.setText(Float.toString(cuenta.getSaldo()));
           String generoSeleccionado = cliente.getSexo();
           if (generoSeleccionado != null) {
                if (generoSeleccionado.equals("Sexo Masculino")) {
                    vista.radioMasculino.setSelected(true);
                 } 
                else if (generoSeleccionado.equals("Sexo Femenino")) {
                    vista.radioFemenino.setSelected(true);
                }
            }
           if(cuenta.getSaldo()!=0){
               vista.txtSaldo.setEnabled(false);
               vista.txtSaldo.setDisabledTextColor(Color.black);
           }
       }
       
   }
    
    
    
    
    
    
    
    
    public static void main(String[] args) {
        
        Cliente cliente = new Cliente();
        CuentaBancaria cuenta = new CuentaBancaria();
        
        dlgBanco vista = new dlgBanco(new JFrame(),true);
        
        Controlador control = new Controlador(cliente, cuenta, vista);
        control.iniciarVista();
    }

}
